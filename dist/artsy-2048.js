/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dev__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__game__ = __webpack_require__(2);



let gameWidth = 4;
let gameHeight = gameWidth;
let score = 0;
let highScore = 0;

// Propgates a 4x4 array with zeros
function emptyGameArray() {
  let gameArray = [];
  for(let x=0; x<gameWidth; x++) {
    gameArray[x] = [];
    for(let y=0; y<gameHeight; y++) {
      gameArray[x][y] = 0;
    }
  }
  return gameArray;
}

//let gameArray = emptyGameArray();
let gameArray = __WEBPACK_IMPORTED_MODULE_0__dev__["a" /* default */].testGameArrays[4];

__WEBPACK_IMPORTED_MODULE_0__dev__["a" /* default */].logGameArray(gameArray);

gameArray = __WEBPACK_IMPORTED_MODULE_1__game__["a" /* default */].nextGameArrayState(gameArray, 3);

__WEBPACK_IMPORTED_MODULE_0__dev__["a" /* default */].logGameArray(gameArray);

let devCanvas = __WEBPACK_IMPORTED_MODULE_0__dev__["a" /* default */].createDevCanvas();
let devCanvasContext = devCanvas.getContext('2d');
document.body.appendChild(devCanvas);

//dev.renderDevCanvasWidthGameArray(devCanvasContext, gameArray);



/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
let testGameArrays = [
  [[2,2,0,0],[2,4,0,8],[0,0,16,16],[32,32,2,0]],
  [[2,0,0,0],[2,0,0,0],[0,0,0,0],[0,0,0,0]],
  [[2,2,2,2],[2,2,2,2],[2,2,2,2],[2,2,2,2]],
  [[64,64,128,128],[32,32,512,512],[1024,1024,1024,1024],[2048,2048,2048,2048]]
];

function logGameArray(gameArray) {
  let toPrint = '';
  for(var x=0; x<gameArray.length; x++) {
    toPrint += x.toString() + ': ';
    for(var y=0; y<gameArray[x].length; y++) {
      toPrint += gameArray[x][y].toString() + ' ';
    }
    toPrint += '\n';
  }
  console.log(toPrint);
}

let devCanvasSize = 512;

function createDevCanvas() {
  let canvas = document.createElement('canvas');

  canvas.id = 'devCanvas';
  canvas.width = devCanvasSize;
  canvas.height = devCanvasSize;
  canvas.position = 'absolute';
  canvas.top = 0;
  canvas.left = 0;

  return canvas;
}

let tileColors = {
  '0': { color: 'gray', text: '', textSize: '0' },
  '2': { color: '#f3c39a', text: '2', textSize: '10' },
  '4': { color: '#d09767', text: '4', textSize: '10' },
  '8': { color: '#ec9e5b', text: '8', textSize: '10' },
  '16': { color: '#e78836', text: '16', textSize: '10' },
  '32': { color: '#e77536', text: '32', textSize: '10' },
  '64': { color: '#f06012', text: '64', textSize: '10' },
  '128': { color: '#f06012', text: '128', textSize: '10' },
  '256': { color: '#f06012', text: '256', textSize: '10' },
  '512': { color: '#f06012', text: '512', textSize: '10' },
  '1024': { color: '#f06012', text: '1024', textSize: '10' },
  '2048': { color: '#f06012', text: '2048', textSize: '10' },
  '4096': { color: '#f06012', text: '4096', textSize: '10' }
};

function renderDevCanvasWidthGameArray(c, gameArray) {
  c.fillStyle = 'white';
  c.fillRect(0,0,devCanvasSize,devCanvasSize);

  c.strokeStyle = 'black';
  for(var x=0; x<gameArray.length; x++) {
    for(var y=0; y<gameArray[x].length; y++) {
      let tileProps = tileColors[gameArray[x][y].toString()];
      let xp = devCanvasSize/gameArray.length;
      let yp = devCanvasSize/gameArray[x].length;

      console.log(c.fillStyle);
      c.fillStyle = tileProps.color;
      c.fillRect(x * xp, y * yp, xp, yp);

      c.font = '20px Helvetica';
      c.fillStyle = 'black';
      c.fillText(tileProps.text, x * xp + 16, y * yp + 32);
    }
  }
}

/* harmony default export */ __webpack_exports__["a"] = ({
  testGameArrays,
  logGameArray,
  createDevCanvas,
  renderDevCanvasWidthGameArray
});


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// Calculates the next state of the game,
// given a direction (0 = Left, 1 = Up, 2 = Right, 3 = Down

function nextGameArrayState(gameArray, direction) {
  return gameArray;
}

/* harmony default export */ __webpack_exports__["a"] = ({ nextGameArrayState });


/***/ })
/******/ ]);