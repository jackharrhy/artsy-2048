import dev from './dev';
import game from './game';

let gameWidth = 4;
let gameHeight = gameWidth;
let score = 0;
let highScore = 0;

// Propgates a 4x4 array with zeros
function emptyGameArray() {
  let gameArray = [];
  for(let x=0; x<gameWidth; x++) {
    gameArray[x] = [];
    for(let y=0; y<gameHeight; y++) {
      gameArray[x][y] = 0;
    }
  }
  return gameArray;
}

//let gameArray = emptyGameArray();
let gameArray = dev.testGameArrays[4];

dev.logGameArray(gameArray);

gameArray = game.nextGameArrayState(gameArray, 3);

dev.logGameArray(gameArray);

let devCanvas = dev.createDevCanvas();
let devCanvasContext = devCanvas.getContext('2d');
document.body.appendChild(devCanvas);

//dev.renderDevCanvasWidthGameArray(devCanvasContext, gameArray);

