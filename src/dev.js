let testGameArrays = [
  [[2,2,0,0],[2,4,0,8],[0,0,16,16],[32,32,2,0]],
  [[2,0,0,0],[2,0,0,0],[0,0,0,0],[0,0,0,0]],
  [[2,2,2,2],[2,2,2,2],[2,2,2,2],[2,2,2,2]],
  [[64,64,128,128],[32,32,512,512],[1024,1024,1024,1024],[2048,2048,2048,2048]]
];

function logGameArray(gameArray) {
  let toPrint = '';
  for(var x=0; x<gameArray.length; x++) {
    toPrint += x.toString() + ': ';
    for(var y=0; y<gameArray[x].length; y++) {
      toPrint += gameArray[x][y].toString() + ' ';
    }
    toPrint += '\n';
  }
  console.log(toPrint);
}

let devCanvasSize = 512;

function createDevCanvas() {
  let canvas = document.createElement('canvas');

  canvas.id = 'devCanvas';
  canvas.width = devCanvasSize;
  canvas.height = devCanvasSize;
  canvas.position = 'absolute';
  canvas.top = 0;
  canvas.left = 0;

  return canvas;
}

let tileColors = {
  '0': { color: 'gray', text: '', textSize: '0' },
  '2': { color: '#f3c39a', text: '2', textSize: '10' },
  '4': { color: '#d09767', text: '4', textSize: '10' },
  '8': { color: '#ec9e5b', text: '8', textSize: '10' },
  '16': { color: '#e78836', text: '16', textSize: '10' },
  '32': { color: '#e77536', text: '32', textSize: '10' },
  '64': { color: '#f06012', text: '64', textSize: '10' },
  '128': { color: '#f06012', text: '128', textSize: '10' },
  '256': { color: '#f06012', text: '256', textSize: '10' },
  '512': { color: '#f06012', text: '512', textSize: '10' },
  '1024': { color: '#f06012', text: '1024', textSize: '10' },
  '2048': { color: '#f06012', text: '2048', textSize: '10' },
  '4096': { color: '#f06012', text: '4096', textSize: '10' }
};

function renderDevCanvasWidthGameArray(c, gameArray) {
  c.fillStyle = 'white';
  c.fillRect(0,0,devCanvasSize,devCanvasSize);

  c.strokeStyle = 'black';
  for(var x=0; x<gameArray.length; x++) {
    for(var y=0; y<gameArray[x].length; y++) {
      let tileProps = tileColors[gameArray[x][y].toString()];
      let xp = devCanvasSize/gameArray.length;
      let yp = devCanvasSize/gameArray[x].length;

      console.log(c.fillStyle);
      c.fillStyle = tileProps.color;
      c.fillRect(x * xp, y * yp, xp, yp);

      c.font = '20px Helvetica';
      c.fillStyle = 'black';
      c.fillText(tileProps.text, x * xp + 16, y * yp + 32);
    }
  }
}

export default {
  testGameArrays,
  logGameArray,
  createDevCanvas,
  renderDevCanvasWidthGameArray
};
